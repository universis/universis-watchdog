
FROM alekzonder/puppeteer:latest

COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json

RUN npm i

COPY --chown=pptruser ./.themost-cli.json /app/.themost-cli.json
COPY --chown=pptruser ./cli-env /app/cli-env
COPY --chown=pptruser ./src /app/src


# RUN chown -R pptuser /app
WORKDIR /app
CMD node /app/src/index.js -g -e /tmp/config.env -f /tmp/input.txt 
