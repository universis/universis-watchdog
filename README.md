# Universis watchdog

A cli tool to identify user errors

## Installation

- `npm install`
- cp ./.env.example ./.env
- Fill the fields at the .env
- The first time you run the scrip, add the `-g` option in order to generate configuration for the themost/cli


## Usage

```md
usage: universis-watchdog [-h] [-v] [-f FILE] [-o OUTPUT] [-g] [--json] [-b]
                          [-d] [-D] [-e ENV]


A cli tool to identify user errors

Optional arguments:
  -h, --help            Show this help message and exit.
  -v, --version         Show program's version number and exit.
  -f FILE, --file FILE  The path of a file with a list of student unique
                        identifiers
  -o OUTPUT, --output OUTPUT
                        The path of the output file
  -g, --generate-config
                        Create the cli configuration
  --json                Make the browser visible
  -b, --browser         Make the browser visible
  -d, --debug           Show errors at the condole
  -D, --dry-run         Run without make any call to external resources or
                        lauch sub processes
  -e ENV, --env ENV     Path to the .env file
```

### In development

`npm start -- -f <__path_to_my_txt_file__>`


### Example execution

```shell

npm start \
-f '/tmp/universis/userIuniqueIdentifiers.txt' \
-o '/tmp/outputfile.json'
```

## Development

### Start universis setup
  - `docker-compose pull` Update images
  - `docker-compose up` start a simple universis environment
  - Students url http://localhost:7001
  - Student account:
    - username: `student1@example.com`
    - password: `abc123ABC!`
