const { exec } = require('child_process');
const fs = require('fs');

/**
 *
 * Parses the output of the themost/cli to usable data
 *
 * @param {string} stdout The output of the cli console
 *
 * @returns {object} The parsed output
 *
 */
const parseCliOutput = (stdout) => {
  const endOfInfo = stdout.substring(stdout.indexOf('The default data types will be loaded instead'));
  let data;
  try {
    const rawData = endOfInfo.substring(endOfInfo.indexOf('['));
    data = JSON.parse(rawData);
  } catch (err) {
    const rawData = endOfInfo.substring(endOfInfo.indexOf('{'));
    data = JSON.parse(rawData);
  }

  return data;
}

/**
 *
 * Resolves the username of the user based on their uniqueIdentifier
 *
 * @param {Array<string>} uniqueIdentifiers The list of the uniqueIdentifiers
 *
 */
const resolveUniqueIdentifier = async (...uniqueIdentifiers) => {
  return new Promise((resolve, reject) => {
    const filters = uniqueIdentifiers
      .map((code) => `uniqueIdentifier eq '${code}'`)
      .join(' or ');
    const cmd = `npx themost cat Student --filter "${filters}" --select "user/name, uniqueIdentifier" --dev`;
    exec(cmd, (err, stdout) => {
      if (err) {
        return reject(err);
      } else {
        const data = parseCliOutput(stdout);
        const formatted = uniqueIdentifiers.map((uniqueIdentifier) => {
          const matchedEntry = data.find((result) => result.uniqueIdentifier === uniqueIdentifier);

          return {
            uniqueIdentifier: uniqueIdentifier,
            username: matchedEntry ? matchedEntry.user_name : undefined
          }
        });
        return resolve(formatted);
      }
    });
  });
};

const getAdapter = (dbConfig) => {
  if (dbConfig.adapter === 'mssql') {
    return {
      "server": dbConfig.host,
      "user": dbConfig.user,
      "password": dbConfig.password,
      "database": dbConfig.database,
      "requestTimeout": 30000
    };
  } else if (dbConfig.adapter === 'sqlite') {
    return {
      "database": dbConfig.database,
    }
  } else {
    throw new Error('Invalid adapter option for dbconfig');
  }
}

const creatConfig = (configPath, dbConfig) => {
  const adapterConfig = getAdapter(dbConfig);
  const cliConfig = {
    "adapterTypes": [
      {
        "name": "MSSQL Data Adapter",
        "invariantName": "mssql",
        "type": "@themost/mssql"
      },
      {
        "name":"SQLite Data Adapter",
        "invariantName": "sqlite",
        "type":"@themost/sqlite"
      }
    ],
    "adapters": [
      {
        "name": "cli_client",
        "default": true,
        "invariantName": dbConfig.adapter,
        "options": adapterConfig
      }
    ]
  };

  fs.writeFileSync(configPath, JSON.stringify(cliConfig, null, 2));
}

module.exports = {
  resolveUniqueIdentifier,
  creatConfig
}
