'use strict';

const puppeteer = require('puppeteer');
const fs = require('fs');

class InteractiveEnvironment {

  constructor(initiator, target, testFunctionList = [], envConfig = {}) {
    this.target = target;
    this.initiator = initiator;
    this.testFunctionList = testFunctionList;
    this.envConfig = envConfig;
    this.status = 'ready';
    this.error = undefined;
    this.impersonationEntryPoint = `${initiator.oauthServer}/auth/admin/master/console`;
  }

  async initialize() {
    const browserArgs = ['--no-sandbox', '--disable-setuid-sandbox'];

    if (this.envConfig.proxy) {
      browserArgs.push(`--proxy-server=${this.envConfig.proxy}`);
    }

    try {
      this.browser = await puppeteer.launch({
        args: browserArgs,
        headless: !(this.envConfig.showBrowser)
      });
      this.page = await this.browser.newPage();
      await this.initiatorLogin();
      this.status = 'initialized';
      await this.impersonateUser();
      await this.screenshot(this.page, 'before_tests_starts');
      this.results = await this.runTestFunctions(this.testFunctionList);
      await this.terminate(this.browser);
      return this.results;
    } catch (err) {
      this.error = 'Error while creating the browser';
      await this.terminate(this.browser);
      throw err;
    }
  }

  async initiatorLogin() {
    if (!this.browser || !this.page) {
      throw new Error('Environment is not initialized');
    }
    try {
      await this.page.goto(this.impersonationEntryPoint, { waitUntil: 'networkidle2' });
      await this.screenshot(this.page, 'impersonationEntryPoint');
      if (this.envConfig.useAUTHImpersonation) {
        await this.page.waitFor('#zocial-AUTH');
        await this.page.click('#zocial-AUTH');
        await this.page.waitForTimeout(1500);
      }

      await this.page.waitFor('#username');
      await this.page.waitFor('#password');
      await this.page.type('#username', this.initiator.username);
      await this.page.type('#password', this.initiator.password);

      await this.page.waitForTimeout(1000);
      await this.screenshot(this.page, 'impersonationEntryPoint_login');
      await this.page.click('input[type=submit]');

      await this.page.waitFor('.navbar');
      await this.screenshot(this.page, 'impersonationMainPage');
    } catch (err) {
      this.error = 'Error during initiator login';
      this.status = 'error';
      await this.terminate(this.browser, 'Error during initiator login');
      throw err;
    }
  }

  async impersonateUser() {
    try {
      await this.page.waitForTimeout(1000);
      await this.page.goto(`${this.initiator.oauthServer}/auth/admin/master/console/#/realms/master/users`, { waitUntil: 'networkidle2' });
      await this.page.waitForTimeout(1000);
      await this.screenshot(this.page, 'initiatorOauthServer');
      await this.page.type('input[type=text]', this.target.username);
      await this.page.click('#userSearch');
      await this.page.waitFor('.clip');
      const impersonationButton = await this.page.$x("//td[contains(text(), 'Impersonate')]");
      await this.screenshot(this.page, 'impersonationSearch');
      await impersonationButton[0].click();
      await new Promise(resolve => setTimeout(resolve, 1000));
      await this.page.waitFor('tbody tr td');
      await this.screenshot(this.page, 'impersonationSearchComplete_page1');
      const targetLink = await this.page.$x(`//a[contains(text(), '${this.target.impersonationLabel}')]`);
      await this.screenshot(this.page, 'impersonationSearchComplete_page1_labelFound');
      await targetLink[0].click();
    } catch (err) {
      this.error = 'Error during impersonation';
      this.status = 'error';
      await this.terminate(this.browser, 'Error during initiator login');
      throw err;
    }
  }

  async runTestFunctions(functionList) {
    const results = [];
    for (let i = 0; i < functionList.length; i++) {
      try {
        const testOutput = await functionList[i](this.page, this.target);
        results.push(testOutput);
      } catch (err) {
        if (this.envConfig.debug) {
          console.error(err);
        }
        results[i] = undefined;
      }
    }
    return results;
  }

  /**
   *
   * Cleans up and reports the results.
   *
   * @param browser The puppeteer browser object
   * @param exitCode The exit code to report
   * @param message The message to show
   */
  async terminate(browser, message) {
    try {
      await browser.disconnect();
      await browser.close();
      this.exitMessage = message;
    } catch (e) {
      if (this.envConfig.debug) {
        console.log("[TechnicalError] Could not close the browser");
      }
    } finally {
      this.status = 'terminated';
    }
  }

  /**
   * 
   * Capture a screenshot of the current view
   * 
   * @param {*} page The reference to the page object
   * @param {string} name The name of the image 
   */
  async screenshot(page, name) {
    if (this.envConfig.debug) {
      const debugDir = 'debug';

      if (!fs.existsSync(debugDir)) {
        fs.mkdirSync(debugDir);
      }

      const date = new Date();
      const dateString = `${date.getFullYear()}-${("0" + date.getMonth()).slice(-2)}-${("0" + date.getDate()).slice(-2)}`;
      const timeString = `${("0" + date.getHours()).slice(-2)}:${("0" + date.getMinutes()).slice(-2)}:${("0" + date.getSeconds()).slice(-2)}`;
      const imageName = `${dateString}_${timeString}_${this.target.username}_${name}.jpg`;
      const path = `./${debugDir}/${imageName}`;

      await page.screenshot({ path, type: 'jpeg' });
    }
  }
}

module.exports = {
  InteractiveEnvironment
}
