#!/usr/bin/env node
'use strict';

const ArgumentParser = require('argparse').ArgumentParser;
const fs = require('fs');
const { InteractiveEnvironment } = require('./interactive-environment/interactive-environment');
const { sipleStudentLogin } = require('./test-functions/test-functions');
const { resolveUniqueIdentifier, creatConfig } = require('./resolvers/themost-cli-resolver');
const { version } = require('./../package.json');

const parser = new ArgumentParser({
  version,
  addHelp: true,
  description: 'A cli tool to identify user errors'
});

parser.addArgument(['-f', '--file'], {
  help: 'The path of a file with a list of student unique identifiers',
  required: false
});
parser.addArgument(['-o', '--output'], {help: 'The path of the output file'});
parser.addArgument(['-g', '--generate-config'], {
  help: 'Create the cli configuration',
  action: 'storeTrue'
});
parser.addArgument(['--json'], {
  help: 'Make the browser visible',
  action: 'storeTrue'
});
parser.addArgument(['-b', '--browser'], {
  help: 'Make the browser visible',
  action: 'storeTrue'
});
parser.addArgument(['-d', '--debug'], {
  help: 'Show errors at the condole',
  action: 'storeTrue'
});
parser.addArgument(['-D', '--dry-run'], {
  help: 'Run without make any call to external resources or lauch sub processes',
  action: 'storeTrue'
});
parser.addArgument(['-e', '--env'], {
  help: 'Path to the .env file'
});

const args = parser.parseArgs();

require('dotenv').config({
  path: args['env']
});

const getArg = (label, environmentLabel, preferEnv=true) => {
  if (process.env[environmentLabel] && preferEnv) {
    return process.env[environmentLabel];
  } else if (args[label]) {
    return process.env[environmentLabel] && preferEnv
      ? process.env[environmentLabel]
      : args[label]
  }

  return undefined;
}

/**
 *
 * Reads input data
 *
 */
const getData = () => {
  let rawInput;

  if (args.file) {
    rawInput = fs.readFileSync(args.file);
  } else {
    rawInput = fs.readFileSync(0, 'utf-8');
  }

  const studentUniqueIdentifiers = rawInput.toString().split('\n')
    .filter((line) => line.length > 0);

  return studentUniqueIdentifiers;
}

/**
 *
 * Filters out the duplicate values of an array
 *
 * @param {Array<string>} uniqueIdentifiers The array to test
 *
 * @returns {Array<string>} The filtered array
 *
 */
const filterUniqueIdentifiers = (uniqueIdentifiers) => {
  const filtered = uniqueIdentifiers.reduce((map, item) => (
    map.indexOf(item) >= 0
      ? map
      : [...map, item]),
    []
  );

  return filtered;
}

/**
 *
 * Sorts the results based on their status and username
 *
 * @param {Array<object>} results The test results
 *
 * @returns {Array<object>} The sorted array
 *
 */
const sortResults = (results) => {
  const sorted = results.sort((a, b) => {
    if (a.result ^ b.result === 0) { // for same result, compare usernames
      return a.username < b.username ? -1 : 1;
    } else { // for different results, compare results
      return a.result < b.result ? -1 : 1;
    }
  });

  return sorted;
}

/**
 *
 * Triggers the script run
 *
 * @param {Array<string>} givenUserCodes The list of unique identifiers to test
 *
 */
const run = (givenUserCodes) => {

  if (getArg('generate_config', 'GENERATE_CONFIG')) {
    creatConfig(`${__dirname}/../cli-env/config/app.json`, {
      host: getArg('host', 'DATABASE_HOST'),
      user: getArg('user', 'DATABASE_USER'),
      password: getArg('password', 'DATABASE_PASSWORD'),
      database: getArg('database', 'DATABASE_NAME'),
      adapter: getArg('adapter', 'DATABASE_ADAPTER')
    });
  }

  const targetUserCodes = filterUniqueIdentifiers(givenUserCodes);

  return new Promise(async(resolve, reject) => {
    const dryRun = getArg('dry_run', 'UNIVERSIS_WATCHDOG_DRY_RUN');
    const debug = getArg('debug', 'UNIVERSIS_WATCHDOG_DEBUG');
    const targetUsers = dryRun
      ? targetUserCodes.map(() => ({
          username: undefined
        }))
      : await resolveUniqueIdentifier(...targetUserCodes);
    const environments = targetUsers
      .map((user) => new InteractiveEnvironment({
        username: getArg('initiatorUsername', 'INITIATOR_USERNAME'),
        password: getArg('initiatorPassword', 'INITIATOR_PASSWORD'),
        oauthServer: getArg('targetOauth', 'INITIATOR_OAUTH')
        }, {
          username: user.username,
          impersonationLabel: getArg('impersonationLabel', 'IMPERSONATION_LABEL'),
          service: getArg('targetService', 'TARGET_SERVICE')
        }, [
          sipleStudentLogin
        ], {
          showBrowser: getArg('browser', 'SHOW_BROWSER'),
          debug: getArg('debug', 'UNIVERSIS_WATCHDOG_DEBUG'),
          useAUTHImpersonation: getArg('oauthStrategy', 'INITIATOR_OAUTH_STRATEGY') === 'AUTH',
          proxy: getArg('proxy', 'UNIVERSIS_WATCHDOG_PROXY')
        })
      );

      const responses = [];
      for (let i =0 ;i < environments.length; i++) {
        const comments = [];
        let testAssertions;
        if (dryRun) {
          comments.push('Dry run');
          testAssertions = [false]
        } else if (!targetUsers[i].username) {
          comments.push('No username');
          testAssertions = [false]
        } else {
          try {
            testAssertions = await environments[i].initialize();
          } catch (err) {
            if (debug) {
              console.error(err);
            }
            testAssertions = [false]
            comments.push('Test execution error');
          }
        }

        responses.push({
            username: targetUsers[i].username,
            uniqueIdentifier: targetUserCodes[i],
            result: testAssertions.every((x) => x),
            assertions: testAssertions,
            comments: comments.join(',')
        });
      }

      const outputFile = getArg('output', 'OUTPUT_FILE');
      const sorted = sortResults(responses);
      const total = responses.length;
      const successful = responses.filter((x) => x.result).length;
      const failed = total - successful;
      const output = {
        total,
        successful,
        failed,
        hasError: failed > 0,
        output: sorted
      }
      if (outputFile) {
        fs.writeFileSync(outputFile, JSON.stringify(output, null, 2));
      } else {
        const asJson = getArg('json', 'UNIVERSIS_WATCHDOG_JSON_FORMAT');
        if (asJson) {
          console.log(JSON.stringify(output, null, 2));
        } else {
          console.table({
            Total: total,
            Successful: successful,
            Failed: failed,
            HasError: failed > 0
          })
          console.table(sorted)
        }
      }
  });
}

const targetUserCodes = getData();
run(targetUserCodes);
